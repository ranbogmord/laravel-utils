<?php

use Illuminate\Support\Facades\Route;
use Ranbogmord\LaravelUtils\Controller\TestController;

Route::get('/', [TestController::class, 'testDefault']);
Route::get('/{file}', [TestController::class, 'testGeneric']);
