<?php

return [
    'dev-route' => [
        'prefix' => 'dev-test',
        'middleware' => ['web'],
        'base_path' => 'dev-routes'
    ]
];
