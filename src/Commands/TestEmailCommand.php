<?php

namespace Ranbogmord\LaravelUtils\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use Ranbogmord\LaravelUtils\Mail\TestEmailMail;

class TestEmailCommand extends Command
{
    public $signature = "test:email {receiver?}";
    public $description = "Sends a test email using the default mail settings";

    /**
     * @return int
     */
    public function handle(): int
    {
        $receiver = $this->argument("receiver");
        if (!$receiver || !filter_var($receiver, FILTER_VALIDATE_EMAIL)) {
            do {
                $receiver = $this->ask("Enter receiver email");
            } while (!$receiver || !filter_var($receiver, FILTER_VALIDATE_EMAIL));
        }

        Mail::to($receiver)->send(new TestEmailMail());

        return 0;
    }
}
