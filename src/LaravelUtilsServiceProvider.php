<?php

namespace Ranbogmord\LaravelUtils;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;
use Ranbogmord\LaravelUtils\Commands\TestEmailCommand;

class LaravelUtilsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'laravel-utils');
    }

    public function boot()
    {
        // TestEmail command setup
        if ($this->app->runningInConsole()) {
            $this->commands([
                TestEmailCommand::class
            ]);

            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path('laravel-utils.php')
            ], 'config');
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'laravel-utils');

        // Developer test routes setup
        if (config('app.debug')) {
            Route::group($this->getRouteConfig(), function () {
                $this->loadRoutesFrom(__DIR__ . '/../routes/test.php');
            });
        }
    }

    private function getRouteConfig(): array
    {
        $middleware = config('laravel-utils.dev-route.middleware', ['web']);
        if (!is_array($middleware)) {
            $middleware = array_filter(explode(',', $middleware));
        }

        return [
            'prefix' => config('laravel-utils.dev-route.prefix', 'dev-test'),
            'middleware' => $middleware
        ];
    }
}
