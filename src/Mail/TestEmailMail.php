<?php

namespace Ranbogmord\LaravelUtils\Mail;

use Illuminate\Mail\Mailable;

class TestEmailMail extends Mailable
{
    public function build(): TestEmailMail
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject("Test email from " . config('app.name'))
            ->text('laravel-utils::mail.test-email');
    }
}
