<?php

namespace Ranbogmord\LaravelUtils\Controller;

use Illuminate\Routing\Controller;

class TestController extends Controller
{
    public function testDefault()
    {
        $filePath = base_path(config('laravel-utils.dev-route.base_path') . '/dev-test.php');
        if (file_exists($filePath)) {
            require_once $filePath;
        } else {
            abort(404);
        }
    }

    public function testGeneric(string $file)
    {
        $filePath = base_path(sprintf("%s/%s.php", config('laravel-utils.dev-route.base_path'), $file));
        if (file_exists($filePath)) {
            require_once $filePath;
        } else {
            abort(404);
        }
    }
}
