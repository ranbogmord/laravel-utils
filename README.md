# Laravel Utils

Small collection of utilities that simplifies day-to-day Laravel development

## Current Tools
- Test Email Command
- Developer test routes

## Installation
Simply install the package from packagist.org
```bash
composer require ranbogmord/laravel-utils
```

## Configuration
Configuration can be published using the following vendor:publish command
```bash
php artisan vendor:publish --provider='Ranbogmord\LaravelUtils\LaravelUtilsServiceProvider' --tag=config
```

## Test Email Command
Allows you to verify that email delivery is working using the configured email setup
```bash
php artisan test:email {receiver?}
```

## Developer test routes
Sets up a "test" route (default: `/dev-test`). This loads a file (default: `base_path/dev-routes/dev-test.php`) where developers can do one-off tests.

It also exposes a more generic route if you want to use multiple test files (default: `/dev-test/{filenameWithoutExtension}`)

These routes are only loaded when `APP_DEBUG` is `true`

### Configuration
|key|description|default|type|
|---|---|---|---|
|`prefix`|The URL prefix for the route|`"dev-test"`|`string`|
|`middleware`|Which middleware to load for the dev routes|`["web"]`|`array` or `string`|
|`base_path`|Path, relative to the Laravel root, from where to load the dev-test files|`"dev-routes"`|`string`|